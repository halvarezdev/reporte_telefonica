import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:reporte_telefonica/Models/serviceModelById.dart';
import 'package:http/http.dart' as http;
import 'dart:developer';

class getAllServicesById extends StatefulWidget {
  const getAllServicesById({Key? key}) : super(key: key);

  @override
  _getAllServicesByIdState createState() => _getAllServicesByIdState();
}

class _getAllServicesByIdState extends State<getAllServicesById> {
  late Future<List<ServiceModelById>> _listadoServiceById;

  Future<List<ServiceModelById>> _getServiceByid() async {
    String id = "1";
    String url = "http://35.193.247.143:8080/report/api/report/$id";
    final response = await http.get(Uri.parse(url));

    List<ServiceModelById> serviceModelById = [];

    if (response.statusCode == 200) {
      var responseBody = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(responseBody);

      for (var item in jsonData) {
        serviceModelById.add(ServiceModelById(
          item["id"],
          item["nameService"],
          item["responseCode"],
          item["countObjectsBody"],
          item["message"],
        ));
      }
      return serviceModelById;
    } else {
      throw Exception("Fallo la conexion");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listadoServiceById = _getServiceByid();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reporte',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              // ignore: prefer_const_constructors
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ),
            title: const Text('Servicios Disponibles'),
          ),
          body: Center(
              child: FutureBuilder(
            future: _listadoServiceById,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView(
                  children:
                      _listService(snapshot.data as List<ServiceModelById>),
                );
              } else if (snapshot.hasError) {
                return const Text('No hay conexión VPN');
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
          ))),
    );
  }

  List<Widget> _listService(List<ServiceModelById> data) {
    List<Widget> listservices = [];

    for (var listservice in data) {
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Id Proyecto: ' + listservice.id.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Nombre Proyecto: ' + listservice.nameService.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Response Code: ' + listservice.responseCode.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
            'CountObjectsBody: ' + listservice.countObjectsBody.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Mensaje: ' + listservice.message.toString()),
      )));
      listservices.add(const Text('\n'));
    }
    return listservices;
  }
}
