import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:reporte_telefonica/Models/serviceModel.dart';
import 'package:http/http.dart' as http;

class getAllServices extends StatefulWidget {
  const getAllServices({Key? key}) : super(key: key);

  @override
  _getAllServicesState createState() => _getAllServicesState();
}

class _getAllServicesState extends State<getAllServices> {
  late Future<List<ServiceModel>> _listadoService;

  Future<List<ServiceModel>> _getService() async {
    String url = 'http://35.193.247.143:8080/report/api/services';
    final response = await http.get(Uri.parse(url));

    List<ServiceModel> serviceModel = [];

    if (response.statusCode == 200) {
      var responseBody = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(responseBody);

      for (var item in jsonData) {
        serviceModel.add(ServiceModel(item["id"], item["url"], item["payload"],
            item["nombre"], item["proyecto_id"], item["state"]));
      }
      return serviceModel;
    } else {
      throw Exception("Fallo la conexion");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listadoService = _getService();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reporte',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              // ignore: prefer_const_constructors
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ),
            title: const Text('Servicios Disponibles'),
          ),
          body: Center(
              child: FutureBuilder(
            future: _listadoService,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView(
                  children: _listService(snapshot.data as List<ServiceModel>),
                );
              } else if (snapshot.hasError) {
                return const Text('No hay conexión VPN');
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
          ))),
    );
  }

  List<Widget> _listService(List<ServiceModel> data) {
    List<Widget> listservices = [];

    for (var listservice in data) {
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Id Proyecto: ' + listservice.id.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Nombre Proyecto: ' + listservice.nombre.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('URL:\n' +
            listservice.url.toString() +
            listservice.payload.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Estado: ' + listservice.state.toString()),
      )));
      listservices.add(const Text('\n'));
    }
    return listservices;
  }
}
