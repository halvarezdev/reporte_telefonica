import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:reporte_telefonica/Models/serviceModel.dart';
import 'package:http/http.dart' as http;
import 'package:reporte_telefonica/Models/serviceModelReport.dart';

// ignore: camel_case_types
class postServices extends StatefulWidget {
  const postServices({Key? key}) : super(key: key);

  @override
  _postServicesState createState() => _postServicesState();
}

// ignore: camel_case_types
class _postServicesState extends State<postServices> {
  Future<List<ServiceModelReport>>? _listadoService;

  Future<List<ServiceModelReport>> _postService() async {
    String url = 'http://35.193.247.143:8080/report/api/report';
    String urlmail = 'http://35.193.247.143:8080/report/api/email';
    dynamic body = jsonEncode([
      {"id": 10, "responseCode": 0},
      {"id": 11, "responseCode": 0},
      {"id": 12, "responseCode": 0},
      {"id": 13, "responseCode": 0},
    ]);
    final response = await http.post(Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: body);

    List<ServiceModelReport> serviceModelReport = [];

    if (response.statusCode == 200) {
      var responseBody = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(responseBody);
 
      
      for (var item in jsonData) {
        serviceModelReport.add(ServiceModelReport(
          item["id"],
          item["nameService"],
          item["responseCode"],
          item["countObjectsBody"],
          item["message"],
        ));

       
      }

  dynamic jsonDataMail = jsonEncode(
{
    "to": ["jecastillo@soaint.com", "halvarez@soaint.com"],
    "cc": ["eremigio@soaint.com"],
    "subject": "[◉]ESTADO DEL MOTOR DE OFERTAS",
    "message": jsonData
}
    );
       final responsemail = await http.post(Uri.parse(urlmail),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonDataMail);

        if (responsemail.statusCode == 200) {
          return serviceModelReport;
        }

      return serviceModelReport;
    } else {
      throw Exception("Fallo la conexion");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _listadoService = _postService();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Reporte',
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              // ignore: prefer_const_constructors
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.pop(context),
            ),
            title: const Text('Servicios Disponibles'),
          ),
          body: Center(
              child: FutureBuilder(
            future: _listadoService,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView(
                  children:
                      _listService(snapshot.data as List<ServiceModelReport>),
                );
              } else if (snapshot.hasError) {
                return const Text('No hay conexión VPN');
              }
              return const Center(
                child: CircularProgressIndicator(),
              );
            },
          ))),
    );
  }

  List<Widget> _listService(List<ServiceModelReport> data) {
    List<Widget> listservices = [];

    for (var listservice in data) {
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Id Proyecto: ' + listservice.id.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Nombre Proyecto: ' + listservice.nameService.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Status Code: ' + listservice.responseCode.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
            'Cantidad de Objetos: ' + listservice.countObjectsBody.toString()),
      )));
      listservices.add(Card(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text('Estado: ' + listservice.message.toString()),
      )));
      listservices.add(const Text('\n'));
    }
    return listservices;
  }
}
