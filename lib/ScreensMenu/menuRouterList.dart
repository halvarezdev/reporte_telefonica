import 'package:flutter/material.dart';
import 'package:reporte_telefonica/ScreensMenu/pages/getAllServices.dart';
import 'package:reporte_telefonica/ScreensMenu/pages/getAllServicesById.dart';
import 'package:reporte_telefonica/ScreensMenu/pages/postServices.dart';

class reportDrawer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return reportDrawerState();
  }
}

// ignore: camel_case_types
class reportDrawerState extends State<reportDrawer> {
  final minimumPadding = 0.00001;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Administración Reporte'),
      ),
      body: const Center(child: Text('Reporte App Elegibles')),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.only(top: minimumPadding, bottom: minimumPadding),
          children: <Widget>[
            DrawerHeader(
              decoration:
                  BoxDecoration(border: Border.all(color: Colors.blueAccent)),
              child: Image.asset(
                'assets/menulogo.png',
                width: 100,
              ),
            ),
            ListTile(
              leading: const Icon(Icons.analytics),
              title: const Text('Reporte Motor'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const postServices()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.receipt_long_outlined),
              title: const Text('Servicios Disponibles'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const getAllServices()));
              },
            ),
            ListTile(
              leading: const Icon(Icons.receipt_long_outlined),
              title: const Text('Servicios por Id Proyecto'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const getAllServicesById()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
