class ServiceModel {
  dynamic id;
  dynamic url;
  dynamic payload;
  dynamic nombre;
  dynamic proyecto_id;
  dynamic state;

  ServiceModel(
    this.id,
    this.url,
    this.payload,
    this.nombre,
    this.proyecto_id,
    this.state,
  ) {
    id = id;
    url = url;
    payload = payload;
    nombre = nombre;
    proyecto_id = proyecto_id;
    state = state;
  }
}
