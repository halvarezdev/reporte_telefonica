class ServiceModelById {
  dynamic id;
  dynamic nameService;
  dynamic responseCode;
  dynamic countObjectsBody;
  dynamic message;

  ServiceModelById(
    this.id,
    this.nameService,
    this.responseCode,
    this.countObjectsBody,
    this.message,
  ) {
    id = id;
    nameService = nameService;
    responseCode = responseCode;
    countObjectsBody = countObjectsBody;
    message = message;
  }
}
